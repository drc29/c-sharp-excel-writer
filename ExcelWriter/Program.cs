﻿using System;
using System.Diagnostics;
using System.IO;
using Bytescout.Spreadsheet;

namespace ExcelWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create new spreadsheet
            Spreadsheet document = new Spreadsheet();

            // add new worksheet
            Worksheet Sheet = document.Workbook.Worksheets.Add("FormulaDemo");

            // headers to include  purpose of the column
            Sheet.Cell("A1").Value = "Formula (as text)";
            //set A column witdh
            Sheet.Columns[0].Width = 250;

            Sheet.Cell("B1").Value = "Formula (calculated)";
            // set B column width
            Sheet.Columns[1].Width = 250;
            // write formula as text 
            Sheet.Cell("A2").Value = "7*3+2";
            // write formula as formula
            Sheet.Cell("B2").Value = "=7*3+2";

            // delete output file if exists already
            if (File.Exists("Output.xlsx"))
            {
                File.Delete("Output.xlsx");
            }

            // Save document
            document.SaveAs("Output.xlsx");

            // Close Spreadsheet
            document.Close();

            // open generated XLS document in default program
            Process.Start("Output.xlsx");
        }
    }
}
